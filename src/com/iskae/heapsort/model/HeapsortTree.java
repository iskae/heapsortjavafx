package com.iskae.heapsort.model;

import java.util.ArrayList;

/**
 * Created by iskae on 20/06/2016.
 */
public class HeapsortTree {
    private HeapsortTreeNode root;
    private ArrayList<HeapsortTreeNode> nodesInTree = new ArrayList<>();

    public void addNodeToTree(HeapsortTreeNode heapsortTreeNode) {
        nodesInTree.add(heapsortTreeNode);
        Integer size = nodesInTree.size();
        String binaryString = Integer.toBinaryString(size);
        heapsortTreeNode.setDepth(binaryString.length() - 1);
        String pathString = binaryString.substring(1);
        if (size >= 2) {
            placeNode(heapsortTreeNode, pathString);
        } else {
            root = heapsortTreeNode;
        }
    }
    private void placeNode(HeapsortTreeNode heapsortTreeNode, String path) {
        char[] pathArray = path.toCharArray();
        HeapsortTreeNode tmpHeapsortTreeNode = root;
        for (int i = 0; i < pathArray.length - 1; i++) {
            if (pathArray[i] == '0') {
                tmpHeapsortTreeNode = tmpHeapsortTreeNode.getLeftChild();
            } else {
                tmpHeapsortTreeNode = tmpHeapsortTreeNode.getRightChild();
            }
        }
        if (pathArray[pathArray.length - 1] == '0') {
            if (pathArray.length != 1) {
                heapsortTreeNode.setParent(tmpHeapsortTreeNode);
                tmpHeapsortTreeNode.setLeftChild(heapsortTreeNode);
            } else {
                heapsortTreeNode.setParent(root);
                root.setLeftChild(heapsortTreeNode);
            }
        } else {
            if (pathArray.length != 1) {
                heapsortTreeNode.setParent(tmpHeapsortTreeNode);
                tmpHeapsortTreeNode.setRightChild(heapsortTreeNode);
            } else {
                heapsortTreeNode.setParent(root);
                root.setRightChild(heapsortTreeNode);
            }
        }
    }


    public ArrayList<HeapsortTreeNode> getNodesInTree() {
        return nodesInTree;
    }

    public void setNodesInTree(ArrayList<HeapsortTreeNode> nodesInTree) {
        this.nodesInTree = nodesInTree;
    }
}
