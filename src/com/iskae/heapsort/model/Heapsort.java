package com.iskae.heapsort.model;

import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by iskae on 14/08/2016.
 */
public class Heapsort {

    private ArrayList<String> changesList;

    public Heapsort() {
        changesList = new ArrayList<>();
    }

    private void swap(ObservableList<HeapsortTreeNode> arr, int a, int b, boolean isSwappedOutOfHeap) {
        String changeString = arr.get(a) + "-" + arr.get(b) + "-" + isSwappedOutOfHeap;
        changesList.add(changeString);
        Collections.swap(arr, a, b);
    }

    public void siftDown(ObservableList<HeapsortTreeNode> arr, int start, int end, boolean descending) {
        int root = start;
        while (root * 2 + 1 <= end) {
            int child = root * 2 + 1;
            int swap = root;
            if (descending) {
                if (arr.get(swap).getKey() > arr.get(child).getKey()) {
                    swap = child;
                }
                if (child + 1 <= end && arr.get(swap).getKey() > arr.get(child + 1).getKey()) {
                    swap = child + 1;
                }
                if (swap != root) {
                    swap(arr, root, swap, false);
                    root = swap;
                } else {
                    return;
                }
            } else {
                if (arr.get(swap).getKey() < arr.get(child).getKey()) {
                    swap = child;
                }
                if (child + 1 <= end && arr.get(swap).getKey() < arr.get(child + 1).getKey()) {
                    swap = child + 1;
                }
                if (swap != root) {
                    swap(arr, root, swap, false);
                    root = swap;
                } else {
                    return;
                }
            }
        }
    }

    private void heapify(ObservableList<HeapsortTreeNode> arr, int i, boolean descending) {
        int start = i / 2 - 1;
        while (start >= 0) {
            siftDown(arr, start, i - 1, descending);
            start -= 1;
        }
    }

    public void sort(ObservableList<HeapsortTreeNode> arr, boolean descending) {
        int count = arr.size();
        heapify(arr, count, descending);
        int end = count - 1;
        while (end > 0) {
            swap(arr, end, 0, true);
            end = end - 1;
            siftDown(arr, 0, end, descending);
        }
    }

    public ArrayList<String> getChangesList() {
        return changesList;
    }
}
