package com.iskae.heapsort.model;

/**
 * Created by iskae on 18/06/2016.
 */
public class HeapsortTreeNode {
    private int key;
    private int depth;

    private HeapsortTreeNode leftChild;
    private HeapsortTreeNode rightChild;
    private HeapsortTreeNode parent;

    public HeapsortTreeNode(int key) {
        this.key = key;
    }

    public HeapsortTreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(HeapsortTreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public HeapsortTreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(HeapsortTreeNode rightChild) {
        this.rightChild = rightChild;
    }

    public int getKey() {
        return key;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public HeapsortTreeNode getParent() {
        return parent;
    }

    public void setParent(HeapsortTreeNode parent) {
        this.parent = parent;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String toString() {
        return String.valueOf(key);
    }

}
