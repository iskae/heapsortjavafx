package com.iskae.heapsort.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by iskae on 04.06.2016.
 */
public class HeapsortList {
    private ArrayList<Integer> heapsortList;

    public HeapsortList() {
        heapsortList = new ArrayList<>();
    }

    public boolean addElementToList(Integer element) {
        if (heapsortList.contains(element)) {
            System.out.println("Element already exists");
            return false;
        } else {
            heapsortList.add(element);
            return true;
        }
    }

    public void removeElementFromList(Integer element) {
        if (heapsortList.contains(element)) {
            heapsortList.remove(element);
        }
    }

    public void generateListWithGivenSize(Integer size) {
        Random random = new Random();
        Set<Integer> listWithNoDuplicates = new HashSet<>();
        while (listWithNoDuplicates.size() < size) {
            int elementToBeAdded = random.nextInt(50) + 1;
            listWithNoDuplicates.add(elementToBeAdded);
        }
        heapsortList = new ArrayList<>(listWithNoDuplicates);
    }

    public ArrayList<Integer> getHeapsortList() {
        return heapsortList;
    }
}
