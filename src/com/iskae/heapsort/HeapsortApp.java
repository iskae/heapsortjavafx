package com.iskae.heapsort;

import com.iskae.heapsort.controller.HsOverviewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

/**
 * Created by iskae on 03.06.2016.
 */
public class HeapsortApp extends Application {
    public static BorderPane rootLayout;

    private Stage primaryStage;


    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("com.iskae.heapsort.HeapsortApp");
        primaryStage.setMaximized(true);

        initRootLayout();

        showOverview();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(HeapsortApp.class.getResource("layout/heapsort/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            MenuBar menuBar = (MenuBar) rootLayout.getTop();
            List menus = menuBar.getMenus();
            Menu menu = (Menu) menus.get(0);
            MenuItem neuItem = menu.getItems().get(0);
            neuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    showOverview();
                }
            });
            MenuItem closeItem = menu.getItems().get(1);
            closeItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Platform.exit();
                }
            });
            Menu help = (Menu) menus.get(1);
            MenuItem about = help.getItems().get(0);
            about.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Über");
                    alert.setHeaderText("Heapsort Visualisierung Applikation");
                    alert.setContentText("Diese Applikation ist für die 5. Semester Verastaltung 'Projektarbeit' " +
                            "unter Betreuung vom Professor Doktor Heiko Körner von Aydin Emre Iskender entwickelt. Sie hat den Zweck  " +
                            ", die Visualisierung der Arbeitsweise des Heapsort-Algorithmus zu zeigen. ");
                    alert.showAndWait();
                }
            });
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(HeapsortApp.class.getResource("layout/heapsort/HeapsortOverview.fxml"));
            VBox heapsortOverview = (VBox) loader.load();
            HsOverviewController controller = loader.getController();
            controller.setRootLayout(rootLayout);
            rootLayout.setCenter(heapsortOverview);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
