package com.iskae.heapsort.controller;

import com.iskae.heapsort.HeapsortApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by iskae on 04.06.2016.
 */
public class HsOverviewController implements Initializable {
    private BorderPane rootLayout;
    private Stage primaryStage;


    @FXML
    protected void manualInputButtonAction() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(HeapsortApp.class.getResource("layout/heapsort/HeapsortManualInputView.fxml"));
        SplitPane manualInputView = (SplitPane) loader.load();
        HsManualInputController controller = loader.getController();
        controller.setSplitPaneLayout(manualInputView);
        controller.setStage(primaryStage);
        rootLayout.setCenter(manualInputView);
    }

    @FXML
    protected void randomListSizeButtonAction() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(HeapsortApp.class.getResource("layout/heapsort/HeapsortRandomListSizeView.fxml"));
        SplitPane randomListSizeView = (SplitPane) loader.load();
        HsRandomListController controller = loader.getController();
        controller.setSplitPaneLayout(randomListSizeView);
        controller.setStage(primaryStage);
        rootLayout.setCenter(randomListSizeView);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setRootLayout(BorderPane rootLayout) {
        this.rootLayout = rootLayout;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
