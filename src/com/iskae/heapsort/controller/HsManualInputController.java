package com.iskae.heapsort.controller;

import com.iskae.heapsort.cells.RectangleCell;
import com.iskae.heapsort.cells.RoundCell;
import com.iskae.heapsort.layout.base.Layout;
import com.iskae.heapsort.layout.tree.TreeLayout;
import com.iskae.heapsort.model.*;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.*;

/**
 * Created by iskae on 19/08/2016.
 */
public class HsManualInputController implements Initializable {
    private SplitPane splitPane;
    private HeapsortList heapsortList = new HeapsortList();
    private Stage primaryStage;
    private ObservableList<HeapsortTreeNode> observableList;
    private boolean startButtonPressed;
    private boolean startButtonAdded;
    private Map<String, Coordinates> coordinatesMap;
    private Map<String, Coordinates> hBoxCoordinatesMap;
    private int lastElementIndex;
    private int lastPlayedAnimation;

    @FXML
    private TextField manualInputField;

    private BorderPane animationLayoutBorder;

    private double xLayoutConstant = 75.0;
    private VBox bottomGroup;
    private Pane initialListPane;
    private Pane pane;
    private Pane resultListPane;
    private ArrayList<Line> edgesInPane;
    private HeapsortTree tree;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lastPlayedAnimation = -1;
        bottomGroup = new VBox();
        bottomGroup.setAlignment(Pos.CENTER);
        bottomGroup.setPadding(new Insets(25, 25, 25, 25));
        bottomGroup.setSpacing(10);
        Label initialListLabel = new Label("Echtzeit Array:");
        initialListPane = new Pane();
        Label resultListLabel = new Label("Ergebnisliste:");
        resultListPane = new Pane();
        bottomGroup.getChildren().addAll(initialListLabel, initialListPane, resultListLabel, resultListPane);
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        double primScreenBoundsWidth = primScreenBounds.getWidth() - 50;
        resultListPane.setPrefWidth(primScreenBoundsWidth);
        initialListPane.setPrefWidth(primScreenBoundsWidth);
        initialListPane.setPadding(new Insets(10, 10, 0, 10));
        initialListPane.setBorder((new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT))));
        resultListPane.setBorder((new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT))));
        resultListPane.setPadding(new Insets(10, 10, 0, 10));
        manualInputField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER) && !startButtonPressed) {
                    String inputText = manualInputField.getCharacters().toString();
                    int inputTextAsInt = Integer.parseInt(inputText);
                    if (inputTextAsInt > 99) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Falsche Eingabe");
                        alert.setHeaderText("Eingabe ist zu viel");
                        alert.setContentText("Bitte geben Sie eine Zahl zwischen 0 und 99 ein.");
                        alert.showAndWait();
                        manualInputField.setText("");
                        return;
                    } else if (heapsortList.getHeapsortList().size() > 14) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Liste zu groß");
                        alert.setHeaderText("Maximale Listegröße errreicht");
                        alert.setContentText("Sie dürfen nicht mehr zahlen einfügen. Bitte starten Sie der Sortiervorgang.");
                        alert.showAndWait();
                        manualInputField.setText("");
                        return;
                    }
                    animationLayoutBorder = (BorderPane) splitPane.getItems().get(1);
                    animationLayoutBorder.setBottom(bottomGroup);

                    boolean isElementAdded = heapsortList.addElementToList(inputTextAsInt);

                    if (isElementAdded) {
                        ArrayList<Integer> numbersInHeapsortList = heapsortList.getHeapsortList();
                        RoundCell roundCell = new RoundCell(numbersInHeapsortList.get(numbersInHeapsortList.size() - 1).toString());
                        initialListPane.getChildren().add(roundCell.getView());
                        RectangleCell rectangleCell = new RectangleCell("");
                        resultListPane.getChildren().add(rectangleCell.getView());
                        double width = primScreenBoundsWidth / initialListPane.getChildren().size();
                        double coeff = 0.0;
                        if (initialListPane.getChildren().size() > 30) {
                            coeff = 5.0;
                        } else if (initialListPane.getChildren().size() > 17) {
                            coeff = 10.0;
                        } else {
                            coeff = width / 2;
                        }
                        for (int i = 0; i < initialListPane.getChildren().size(); i++) {
                            if (initialListPane.getChildren().get(i) instanceof StackPane) {
                                StackPane stackPane = (StackPane) initialListPane.getChildren().get(i);
                                stackPane.relocate(((width * (i)) + coeff), 10);
                            }
                        }
                        for (int i = 0; i < resultListPane.getChildren().size(); i++) {
                            StackPane stackPane = (StackPane) resultListPane.getChildren().get(i);
                            stackPane.relocate(((width * (i)) + coeff), 10);
                        }
                        manualInputField.setText("");
                        if (heapsortList.getHeapsortList().size() > 1) {
                            if (!startButtonAdded) {
                                startButtonAdded = true;
                                Button startAnimationButton = new Button("Start Sortverfahren");
                                CheckBox descendingOrderCheckbox = new CheckBox("Absteigend");
                                CheckBox debugModeCheckbox = new CheckBox("Debug Modus");
                                ArrayList<SequentialTransition> transitions = new ArrayList<SequentialTransition>();
                                debugModeCheckbox.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (debugModeCheckbox.isSelected()) {
                                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                            alert.setTitle("Debug Modus aktiviert");
                                            alert.setHeaderText("Heapsort-Verfahren im Debug Modus");
                                            alert.setContentText("In diesem Modus wird der Heapsort-Verfahren Schritt für Schritt gezeigt. " +
                                                    "Falls Sie diesem Modus einschalten, müssen Sie der 'Spiel' Button anklicken, um der nachste Schritt auszuführen");
                                            alert.showAndWait();
                                        }
                                    }
                                });
                                descendingOrderCheckbox.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (descendingOrderCheckbox.isSelected()) {
                                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                            alert.setTitle("Absteigend aktiviert");
                                            alert.setHeaderText("Absteigendes Heapsort-Verfahren");
                                            alert.setContentText("Normalerweise wird das Array in Aufsteigender Folge sortiert." +
                                                    "Mit der Aktivierung dieser Checkbox wird das Array in Absteigender Folge sortiert.");
                                            alert.showAndWait();
                                        }
                                    }
                                });
                                HBox animationControlLayout = (HBox) splitPane.getItems().get(0);
                                animationControlLayout.getChildren().add(descendingOrderCheckbox);
                                animationControlLayout.getChildren().add(debugModeCheckbox);
                                animationControlLayout.getChildren().add(startAnimationButton);
                                pane = new Pane();
                                animationLayoutBorder.setCenter(pane);
                                startAnimationButton.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (!startButtonPressed) {
                                            tree = new HeapsortTree();
                                            for (Integer i : numbersInHeapsortList) {
                                                HeapsortTreeNode heapsortTreeNode = new HeapsortTreeNode(i);
                                                tree.addNodeToTree(heapsortTreeNode);
                                            }
                                            drawTree(tree, pane);
                                            edgesInPane = TreeLayout.getEdgesList();
                                            startButtonPressed = true;
                                            observableList = FXCollections.observableList(tree.getNodesInTree());
                                            Heapsort sorter = new Heapsort();
                                            sorter.sort(observableList, descendingOrderCheckbox.isSelected());
                                            ArrayList<String> changesList = sorter.getChangesList();
                                            coordinatesMap = generateCoordinatesMap(pane);
                                            hBoxCoordinatesMap = generateHboxMap(initialListPane);
                                            lastElementIndex = numbersInHeapsortList.size() - 1;
                                            if (!debugModeCheckbox.isSelected()) {
                                                for (String change : changesList) {
                                                    transitions.add(buildTransition(change));
                                                }
                                                SequentialTransition seqTrans = new SequentialTransition();
                                                for (SequentialTransition transition : transitions) {
                                                    for (int i = 0; i < transition.getChildren().size(); i++) {
                                                        seqTrans.getChildren().add(transition.getChildren().get(i));
                                                    }
                                                }
                                                seqTrans.play();
                                                seqTrans.setOnFinished(event1 -> playLastTransition(pane, resultListPane));
                                            } else {
                                                animationControlLayout.getChildren().removeAll(animationControlLayout.getChildren());
                                                Image nextImage = new Image(getClass().getResourceAsStream("/images/next.png"));
                                                Button nextButton = new Button("Weiter", new ImageView(nextImage));
                                                nextButton.setOnAction(new EventHandler<ActionEvent>() {
                                                    @Override
                                                    public void handle(ActionEvent event) {
                                                        lastPlayedAnimation++;
                                                        if (lastPlayedAnimation < changesList.size()) {
                                                            nextButton.setDisable(true);
                                                            SequentialTransition seqTrans = buildTransition(changesList.get(lastPlayedAnimation));
                                                            seqTrans.setOnFinished(finishHim -> nextButton.setDisable(false));
                                                            seqTrans.play();
                                                        } else {
                                                            playLastTransition(pane, resultListPane);
                                                        }
                                                    }
                                                });
                                                animationControlLayout.getChildren().add(nextButton);

                                            }
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Fehler");
                        alert.setHeaderText("Dieses Element ist schon vorhanden");
                        alert.setContentText("Der von Ihnen eingegebene Element ist schon in der Liste eingetragen. Bitte geben Sie ein andere Zahl ein.");
                        manualInputField.setText("");
                        alert.showAndWait();
                    }
                }
            }
        });

    }

    private SequentialTransition buildTransition(String change) {
        SequentialTransition heapsortPartTransition = new SequentialTransition();
        String[] changeSplitted = change.split("-");
        String firstNumber = changeSplitted[0];
        String secondNumber = changeSplitted[1];
        boolean isRemovedFromHeap = Boolean.valueOf(changeSplitted[2]);
        StackPane firstPane = findPane(firstNumber, pane);
        StackPane secondPane = findPane(secondNumber, pane);
        StackPane firstPaneInHbox = findPane(firstNumber, initialListPane);
        StackPane secondPaneInHbox = findPane(secondNumber, initialListPane);
        double firstPaneInHboxLayoutX = hBoxCoordinatesMap.get(firstNumber).getX();
        double secondPaneInHboxLayoutX = hBoxCoordinatesMap.get(secondNumber).getX();
        double firstPaneInHboxLayoutY = hBoxCoordinatesMap.get(firstNumber).getY();
        double secondPaneInHboxLayoutY = hBoxCoordinatesMap.get(secondNumber).getY();
        final KeyValue firstCellInHboxXValue = new KeyValue(firstPaneInHbox.layoutXProperty(), secondPaneInHboxLayoutX);
        final KeyValue firstCellInHboxYValue = new KeyValue(firstPaneInHbox.layoutYProperty(), firstPaneInHboxLayoutY);
        final KeyValue secondCellInHboxXValue = new KeyValue(secondPaneInHbox.layoutXProperty(), firstPaneInHboxLayoutX);
        final KeyValue secondCellInHboxYValue = new KeyValue(secondPaneInHbox.layoutYProperty(), secondPaneInHboxLayoutY);
        double firstPaneLayoutX = coordinatesMap.get(firstNumber).getX();
        double secondPaneLayoutX = coordinatesMap.get(secondNumber).getX();
        double firstPaneLayoutY = coordinatesMap.get(firstNumber).getY();
        double secondPaneLayoutY = coordinatesMap.get(secondNumber).getY();
        Timeline timeline = new Timeline();
        if (!isRemovedFromHeap) {
            final KeyValue firstCellXValue = new KeyValue(firstPane.layoutXProperty(), secondPaneLayoutX);
            final KeyValue firstCellYValue = new KeyValue(firstPane.layoutYProperty(), secondPaneLayoutY);
            final KeyValue secondCellXValue = new KeyValue(secondPane.layoutXProperty(), firstPaneLayoutX);
            final KeyValue secondCellYValue = new KeyValue(secondPane.layoutYProperty(), firstPaneLayoutY);
            final KeyFrame kf = new KeyFrame(Duration.millis(2000),
                    firstCellXValue, firstCellYValue, secondCellXValue, secondCellYValue,
                    firstCellInHboxXValue, firstCellInHboxYValue, secondCellInHboxXValue, secondCellInHboxYValue);
            timeline.getKeyFrames().add(kf);
        } else {
            final KeyValue firstCellXValue = new KeyValue(firstPane.layoutXProperty(), secondPaneLayoutX);
            final KeyValue firstCellYValue = new KeyValue(firstPane.layoutYProperty(), secondPaneLayoutY);
            final KeyFrame kf = new KeyFrame(Duration.millis(2000),
                    firstCellXValue, firstCellYValue,
                    firstCellInHboxXValue, firstCellInHboxYValue, secondCellInHboxXValue, secondCellInHboxYValue);
            timeline.getKeyFrames().add(kf);
            StackPane rectanglePane = (StackPane) resultListPane.getChildren().get(lastElementIndex);
            Text rectanglePaneText = (Text) rectanglePane.getChildren().get(1);
            Line edgeToBeRemoved = findEdgeInPane(edgesInPane, pane);
            Timeline resultBoxTimeline = new Timeline();
            final KeyValue resultCellValue = new KeyValue(rectanglePaneText.textProperty(), secondPane.getId());
            final KeyFrame resultCellKeyFrame = new KeyFrame(Duration.millis(2000),
                    resultCellValue);
            resultBoxTimeline.getKeyFrames().add(resultCellKeyFrame);
            ParallelTransition pl = new ParallelTransition(removeFadingOut(secondPane, pane), resultBoxTimeline, removeFadingOut(edgeToBeRemoved, pane));
            heapsortPartTransition.getChildren().add(pl);
            lastElementIndex--;
        }
        heapsortPartTransition.getChildren().add(addMarkedTransition(firstPane, secondPane, firstPaneInHbox, secondPaneInHbox));
        HeapsortTreeNode parentNode = findNodeInTree(firstNumber, tree);
        if (parentNode.getLeftChild() != null && parentNode.getRightChild() != null && !isRemovedFromHeap) {
            Label comparisonLabel = new Label();
            comparisonLabel.setLayoutX(firstPaneLayoutX);
            comparisonLabel.setLayoutY(secondPaneLayoutY - 15);
            if (parentNode.getLeftChild().getKey() > parentNode.getRightChild().getKey()) {
                comparisonLabel.setText(">");
            } else {
                comparisonLabel.setText("<");
            }
            comparisonLabel.setFont(Font.font("Verdana", 50));
            FadeTransition fadeInTransition = addFadingIn(comparisonLabel, pane);
            FadeTransition fadeOutTransition = removeFadingOut(comparisonLabel, pane);
            heapsortPartTransition.getChildren().add(fadeInTransition);
            heapsortPartTransition.getChildren().add(fadeOutTransition);
            heapsortPartTransition.getChildren().add(timeline);
        } else {
            heapsortPartTransition.getChildren().add(timeline);
        }
        heapsortPartTransition.getChildren().add(addUnmarkedTransition(firstPane, secondPane, firstPaneInHbox, secondPaneInHbox));
        switchOnTree(firstNumber, secondNumber, tree);
        updateMap(firstNumber, new Coordinates(secondPaneLayoutX, secondPaneLayoutY));
        updateMap(secondNumber, new Coordinates(firstPaneLayoutX, firstPaneLayoutY));
        updateHBoxMap(firstNumber, new Coordinates(secondPaneInHboxLayoutX, secondPaneInHboxLayoutY));
        updateHBoxMap(secondNumber, new Coordinates(firstPaneInHboxLayoutX, firstPaneInHboxLayoutY));
        return heapsortPartTransition;
    }

    private Line findEdgeInPane(ArrayList<Line> edges, Pane pane) {
        List childrenOfPane = pane.getChildren();
        Line lastLine = edges.get(lastElementIndex - 1);
        for (Object node : childrenOfPane) {
            if (node instanceof Line) {
                Line line = (Line) node;
                if (line.equals(lastLine)) {
                    return line;
                }
            }
        }
        return null;
    }

    private void playLastTransition(Pane pane, Pane resultListPane) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                StackPane lastPane = (StackPane) pane.getChildren().get(0);
                StackPane rectanglePane = (StackPane) resultListPane.getChildren().get(lastElementIndex);
                Text rectanglePaneText = (Text) rectanglePane.getChildren().get(1);
                Timeline resultBoxTimeline = new Timeline();
                final KeyValue resultCellValue = new KeyValue(rectanglePaneText.textProperty(), lastPane.getId());
                final KeyFrame resultCellKeyFrame = new KeyFrame(Duration.millis(2000),
                        resultCellValue);
                resultBoxTimeline.getKeyFrames().add(resultCellKeyFrame);
                ParallelTransition lastTransition = new ParallelTransition(resultBoxTimeline, removeFadingOut(lastPane, pane));
                lastTransition.play();
                lastTransition.setOnFinished(event1 -> finished());
            }
        });
    }

    public ParallelTransition addMarkedTransition(StackPane firstPane, StackPane secondPane, StackPane firstPaneInHbox, StackPane secondPaneInHbox) {
        FillTransition firstPaneFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(firstPane), Color.WHITE, Color.RED);
        FillTransition secondPaneFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(secondPane), Color.WHITE, Color.RED);
        FillTransition firstPaneHboxFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(firstPaneInHbox), Color.WHITE, Color.RED);
        FillTransition secondPaneHboxFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(secondPaneInHbox), Color.WHITE, Color.RED);
        ParallelTransition pl = new ParallelTransition(firstPaneFillTransition, secondPaneFillTransition, firstPaneHboxFillTransition, secondPaneHboxFillTransition);
        return pl;
    }

    public ParallelTransition addUnmarkedTransition(StackPane firstPane, StackPane secondPane, StackPane firstPaneInHbox, StackPane secondPaneInHbox) {
        FillTransition firstPaneFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(firstPane), Color.RED, Color.WHITE);
        FillTransition secondPaneFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(secondPane), Color.RED, Color.WHITE);
        FillTransition firstPaneHboxFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(firstPaneInHbox), Color.RED, Color.WHITE);
        FillTransition secondPaneHboxFillTransition = new FillTransition(Duration.millis(500), getCircleOfStackPane(secondPaneInHbox), Color.RED, Color.WHITE);
        ParallelTransition pl = new ParallelTransition(firstPaneFillTransition, secondPaneFillTransition, firstPaneHboxFillTransition, secondPaneHboxFillTransition);
        return pl;
    }

    public FadeTransition addFadingIn(final Node node, final Pane parent) {
        final FadeTransition transition = new FadeTransition(Duration.millis(1000), node);
        transition.setFromValue(0);
        transition.setToValue(1);
        transition.setInterpolator(Interpolator.EASE_IN);
        parent.getChildren().add(node);
        return transition;
    }

    public FadeTransition removeFadingOut(final Node node, final Pane parent) {
        if (parent.getChildren().contains(node)) {
            final FadeTransition transition = new FadeTransition(Duration.millis(1000), node);
            transition.setFromValue(node.getOpacity());
            transition.setToValue(0);
            transition.setInterpolator(Interpolator.EASE_BOTH);
            transition.setOnFinished(finishHim -> {
                parent.getChildren().remove(node);
            });
            return transition;
        }
        return null;
    }

    private Circle getCircleOfStackPane(StackPane pane) {
        List childrenOfPane = pane.getChildren();
        for (Object node : childrenOfPane) {
            if (node instanceof Circle) {
                Circle circle = (Circle) node;
                return circle;
            }
        }
        return null;
    }


    private Map<String, Coordinates> generateCoordinatesMap(Pane pane) {
        Map<String, Coordinates> result = new HashMap<>();
        List childrenOfPane = pane.getChildren();
        for (Object node : childrenOfPane) {
            if (node instanceof StackPane) {
                StackPane stackPane = (StackPane) node;
                result.put(stackPane.getId(), new Coordinates(stackPane.getLayoutX(), stackPane.getLayoutY()));
            }
        }
        return result;
    }

    private Map<String, Coordinates> generateHboxMap(Pane initialList) {
        Map<String, Coordinates> result = new HashMap<>();
        List childrenOfPane = initialList.getChildren();
        for (Object node : childrenOfPane) {
            if (node instanceof StackPane) {
                StackPane stackPane = (StackPane) node;
                result.put(stackPane.getId(), new Coordinates(stackPane.getLayoutX(), stackPane.getLayoutY()));
            }
        }
        return result;
    }

    private void updateMap(String id, Coordinates newValue) {
        Coordinates oldValue = coordinatesMap.get(id);
        if (oldValue != null) {
            coordinatesMap.put(id, newValue);
        } else {
            System.out.println("old value cannot be null");
        }
    }

    private void updateHBoxMap(String id, Coordinates newValue) {
        Coordinates oldValue = hBoxCoordinatesMap.get(id);
        if (oldValue != null) {
            hBoxCoordinatesMap.put(id, newValue);
        } else {
            System.out.println("old value cannot be null");
        }
    }

    private StackPane findPane(String number, Pane pane) {
        List childrenOfPane = pane.getChildren();
        for (Object node : childrenOfPane) {
            if (node instanceof StackPane) {
                StackPane stackPane = (StackPane) node;
                if (stackPane.getId().equals(number)) {
                    return stackPane;
                }
            }
        }
        return null;
    }

    private void drawTree(HeapsortTree tree, Pane pane) {
        ArrayList<HeapsortTreeNode> nodesInTree = tree.getNodesInTree();
        for (HeapsortTreeNode node : nodesInTree) {
            pane.getChildren().add(new RoundCell(String.valueOf(node.getKey())).getView());
        }

        Layout layout = new TreeLayout(pane, tree);
        layout.execute();
    }

    public void setSplitPaneLayout(SplitPane splitPane) {
        this.splitPane = splitPane;
    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void finished() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sortierverfahren beendet");
                alert.setHeaderText("Infomeldung");
                alert.setContentText("Das Sortierverfahren ist beendet." +
                        "Sie können das sortierte Array in der Ansicht" +
                        "sehen. Um neu anzufangen, bitte starten Sie das Programm neu mit dem Befehl" +
                        "Menü -> Neu");
                alert.showAndWait();
            }
        });
    }

    private HeapsortTreeNode findParentNode(String firstNumber, HeapsortTree tree) {
        List<HeapsortTreeNode> nodesInTree = tree.getNodesInTree();
        for (HeapsortTreeNode node : nodesInTree) {
            if (String.valueOf(node.getKey()).equals(firstNumber)) {
                return node;
            }
        }
        return null;
    }

    private void switchOnTree(String firstNumber, String secondNumber, HeapsortTree tree) {
        HeapsortTreeNode firstNode = findNodeInTree(firstNumber, tree);
        HeapsortTreeNode secondNode = findNodeInTree(secondNumber, tree);
        if (firstNode != null && secondNode != null) {
            int firstKey = Integer.parseInt(firstNumber);
            int secondKey = Integer.parseInt(secondNumber);
            firstNode.setKey(secondKey);
            secondNode.setKey(firstKey);
        }
    }

    private HeapsortTreeNode findNodeInTree(String firstNumber, HeapsortTree tree) {
        List<HeapsortTreeNode> nodesInTree = tree.getNodesInTree();
        for (HeapsortTreeNode node : nodesInTree) {
            if (String.valueOf(node.getKey()).equals(firstNumber)) {
                return node;
            }
        }
        return null;
    }
}
