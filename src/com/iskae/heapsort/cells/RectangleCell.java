package com.iskae.heapsort.cells;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by iskae on 29/08/2016.
 */
public class RectangleCell {
    private StackPane view;
    private String number;

    public RectangleCell(String number) {
        this.number = number;
        view = createRectNumber(number);
    }

    public StackPane createRectNumber(String number) {
        Rectangle rectangle= new Rectangle();
        Text text = new Text(number);
        text.setBoundsType(TextBoundsType.VISUAL);
        rectangle.setFill(Color.WHITE);
        rectangle.setStroke(Color.BLACK);
        rectangle.setHeight(30);
        rectangle.setWidth(40);
        StackPane stack = new StackPane(rectangle, text);
        stack.setId(number);
        return stack;
    }

    public StackPane getView() {
        return view;
    }

    public String getNumber() {
        return number;
    }
}
