package com.iskae.heapsort.cells;

import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

public class RoundCell extends Pane {
    private StackPane view;
    private String number;
    private Circle circle;
    private Text text;

    public RoundCell(String number) {
        this.number = number;
        view = createRounderNumber(number);
    }

    public StackPane createRounderNumber(String number) {
        circle = new Circle();
        text = new Text(number);
        text.setBoundsType(TextBoundsType.VISUAL);
        circle.setFill(Color.WHITE);
        circle.setStroke(Color.BLACK);
        circle.setRadius(20);
        StackPane stack = new StackPane(circle, text);
        stack.setId(number);
        return stack;
    }

    public StackPane getView() {
        return view;
    }

    public String getNumber() {
        return number;
    }

    public Circle getCircle() {
        return circle;
    }

}