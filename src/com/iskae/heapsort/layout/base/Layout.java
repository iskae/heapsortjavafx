package com.iskae.heapsort.layout.base;

public abstract class Layout {

    public abstract void execute();

}