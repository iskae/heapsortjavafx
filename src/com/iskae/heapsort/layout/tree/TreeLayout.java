package com.iskae.heapsort.layout.tree;

import com.iskae.heapsort.layout.base.Layout;
import com.iskae.heapsort.model.HeapsortTree;
import com.iskae.heapsort.model.HeapsortTreeNode;
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.stage.Screen;

import java.util.ArrayList;
import java.util.List;

public class TreeLayout extends Layout {
    private Pane pane;
    private HeapsortTree tree;
    private static ArrayList<Line> edgesList;

    public TreeLayout(Pane pane, HeapsortTree tree) {
        this.pane = pane;
        this.tree = tree;
        this.edgesList = new ArrayList<>();
    }

    public void execute() {
        ArrayList<HeapsortTreeNode> nodesInTree = tree.getNodesInTree();
        int pointer = 0;
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        double width = primScreenBounds.getWidth();
        double depthY = 100;
        while (pointer < nodesInTree.size()) {
            int numberOfNodesToBeMarked = (int) Math.pow(2, nodesInTree.get(pointer).getDepth());
            int numberOfNodesAlreadyMarked = 0;
            int pointerXPos = 1;
            while (numberOfNodesAlreadyMarked < numberOfNodesToBeMarked && pointer < nodesInTree.size()) {
                HeapsortTreeNode node = nodesInTree.get(pointer);
                StackPane cellOfTreeNode = getCellOfTreeNode(node, pane);
                double xPosToBeRelocated = ((width / (numberOfNodesToBeMarked * 2)) * pointerXPos) - 25;
                double yPosToBeRelocated = ((depthY * nodesInTree.get(pointer).getDepth()) + 10);
                cellOfTreeNode.relocate(xPosToBeRelocated, yPosToBeRelocated);
                numberOfNodesAlreadyMarked++;
                pointer++;
                pointerXPos += 2;
            }
        }
        for (HeapsortTreeNode node : nodesInTree) {
            StackPane parentNode = getCellOfTreeNode(node, pane);
            if (node.getLeftChild() != null) {
                StackPane leftChild = getCellOfTreeNode(node.getLeftChild(), pane);
                Line line = new Line();
                line.setStartX(parentNode.getLayoutX() + 20);
                line.setStartY(parentNode.getLayoutY() + 40);
                line.setEndX(leftChild.getLayoutX() + 20);
                line.setEndY(leftChild.getLayoutY());
                edgesList.add(line);
                pane.getChildren().add(line);
            }
            if (node.getRightChild() != null) {
                StackPane rightChild = getCellOfTreeNode(node.getRightChild(), pane);
                Line line = new Line();
                line.setStartX(parentNode.getLayoutX() + 20);
                line.setStartY(parentNode.getLayoutY() + 40);
                line.setEndX(rightChild.getLayoutX() + 20);
                line.setEndY(rightChild.getLayoutY());
                edgesList.add(line);
                pane.getChildren().add(line);
            }
        }
    }

    public StackPane getCellOfTreeNode(HeapsortTreeNode node, Pane pane) {
        List childrenOfPane = pane.getChildren();
        for (int i = 0; i < childrenOfPane.size(); i++) {
            StackPane cell = (StackPane) childrenOfPane.get(i);
            if (cell.getId().equals(String.valueOf(node.getKey()))) {
                return cell;
            }
        }
        return null;
    }

    public static  ArrayList<Line> getEdgesList() {
        return edgesList;
    }
}